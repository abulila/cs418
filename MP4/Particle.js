class Particle {
    constructor() {
        this.position = [0.0,0.0,0.0];
        this.velocity = [0.0,0.0,0.0];
        this.acceleration = [0.0,0.0,0.0];

        this.color = [0.0,0.0,0.0];
        this.radius = 1.0;
        this.mass = 1.0;
    }

    set_position(x, y, z) {
        this.position = [x,y,z];
    }

    set_velocity(vx, vy, vz) {
        this.velocity = [vx,vy,vz];
    }

    set_acceleration(ax,ay,az) {
        this.acceleration = [ax,ay,az];
    }

    set_color(r, g, b) {
        this.color = [r, g, b];
    }

    set_radius(radius) {
        this.radius = radius;
    }

    set_mass(mass) {
        this.mass = mass;
    }


}
