/**
 * @fileoverview SkyBox - A simple 3D texture cube using WebGL
 * @author Eric Roch
 */

/** Class implementing texture cube map. */
class SkyBox {
    /**
     * Initialize members of a SkyBox object
     */
    constructor() {
        // default texture image names
        this.defaultFileNames = ["images/pos-z.png", "images/neg-z.png",
                                 "images/pos-y.png", "images/neg-y.png",
                                 "images/pos-x.png", "images/neg-x.png"];

        // Images for skybox texture
        this.cubeImage0;
        this.cubeImage1;
        this.cubeImage2;
        this.cubeImage3;
        this.cubeImage4;
        this.cubeImage5;
        this.cubeImages = [this.cubeImage0, this.cubeImage1, this.cubeImage2, this.cubeImage3, this.cubeImage4, this.cubeImage5];
        this.cubeMap;

        this.texturesLoaded = 0;
        this.numFaces = 0;
        this.numVertices = 0;
        this.cubeLoaded = false

        // Allocate buffers for actual skybox
        this.vBuffer = [];
        this.fBuffer = [];
        this.nBuffer = [];
        this.eBuffer = [];

        console.log("SkyBox: Allocated buffers");
    }

    loaded() {
        return (this.texturesLoaded == 6 && this.cubeLoaded);
    }

    loadImages(filenames = this.defaultFileNames) {
        this.cubeMap = gl.createTexture();

        for (var i = 0; i < 6; i++) {
            this.setupPromise(filenames[i], i);
        }
    }

    setupPromise(filename, face) {
        var myPromise = this.asyncGetTextureFile(filename, face);
        // We define what to do when the promise is resolved with the then() call,
        // and what to do when the promise is rejected with the catch() call
        myPromise.then((status) => {
            console.log("SkyBox: Successfully retrieved file:", filename);
            this.handleTextureLoaded(this.cubeImages[face], face)
        })
        .catch((reason) => {
            // Log the rejection reason
            console.log("SkyBox: Failed to retrieve file:", reason);
        });
    }

    asyncGetTextureFile(url, face) {
        console.log("SkyBox: Getting image file:", url);
        return new Promise((resolve, reject) => {
            this.cubeImages[face] = new Image();
            this.cubeImages[face].onload = () => resolve({url, status: 'ok'});
            this.cubeImages[face].onerror = () => reject({url, status: 'error'});
            this.cubeImages[face].src = url;
            console.log("SkyBox: Made promise");
        });
    }

    /**
     * Texture handling. Generates mipmap and sets texture parameters.
     * @param {Object} image Image for cube application
     * @param {Number} face Which face of the cubeMap to add texture to
     */
    handleTextureLoaded(image, face) {
        console.log("SkyBox: Loading texture:", image);
        this.texturesLoaded++;

        var cubeMapFace;
        switch (face) {
            case 0: cubeMapFace = gl.TEXTURE_CUBE_MAP_POSITIVE_Z; break;
            case 1: cubeMapFace = gl.TEXTURE_CUBE_MAP_NEGATIVE_Z; break;
            case 2: cubeMapFace = gl.TEXTURE_CUBE_MAP_POSITIVE_Y; break;
            case 3: cubeMapFace = gl.TEXTURE_CUBE_MAP_NEGATIVE_Y; break;
            case 4: cubeMapFace = gl.TEXTURE_CUBE_MAP_POSITIVE_X; break;
            case 5: cubeMapFace = gl.TEXTURE_CUBE_MAP_NEGATIVE_X; break;
            default:
                console.log("SkyBox: Invalid face:", face);
                return;
        }

        gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.cubeMap);
        gl.texImage2D(cubeMapFace, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

        // Clamping
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        // Filtering
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    }


    generateCube() {
        // this.vBuffer.push(-1, -1, -1);  // Left - Front - Bottom
        // this.vBuffer.push(-1, -1,  1);  // Left - Front - Top
        // this.vBuffer.push(-1,  1, -1);  // Left - Back - Bottom
        // this.vBuffer.push(-1,  1,  1);  // Left - Back - Top
        // this.vBuffer.push( 1, -1, -1);  // Right - Front - Bottom
        // this.vBuffer.push( 1, -1,  1);  // Right - Front - Top
        // this.vBuffer.push( 1,  1, -1);  // Right - Back - Bottom
        // this.vBuffer.push( 1,  1,  1);  // Right - Back - Top
        // this.numVertices = 8;
        this.vBuffer = [
          // Front face
          -1.0, -1.0,  1.0,
           1.0, -1.0,  1.0,
           1.0,  1.0,  1.0,
          -1.0,  1.0,  1.0,

          // Back face
          -1.0, -1.0, -1.0,
          -1.0,  1.0, -1.0,
           1.0,  1.0, -1.0,
           1.0, -1.0, -1.0,

          // Top face
          -1.0,  1.0, -1.0,
          -1.0,  1.0,  1.0,
           1.0,  1.0,  1.0,
           1.0,  1.0, -1.0,

          // Bottom face
          -1.0, -1.0, -1.0,
           1.0, -1.0, -1.0,
           1.0, -1.0,  1.0,
          -1.0, -1.0,  1.0,

          // Right face
           1.0, -1.0, -1.0,
           1.0,  1.0, -1.0,
           1.0,  1.0,  1.0,
           1.0, -1.0,  1.0,

          // Left face
          -1.0, -1.0, -1.0,
          -1.0, -1.0,  1.0,
          -1.0,  1.0,  1.0,
          -1.0,  1.0, -1.0
        ];
        this.numVertices = this.vBuffer.length/3;

        // Add triangles to face buffer
        // this.fBuffer.push(0, 1, 2);     // Left 1
        // this.fBuffer.push(1, 2, 3);     // Left 2
        // this.fBuffer.push(4, 5, 6);     // Right 1
        // this.fBuffer.push(5, 6, 7);     // Right 2
        // this.fBuffer.push(0, 1, 4);     // Front 1
        // this.fBuffer.push(1, 4, 5);     // Front 2
        // this.fBuffer.push(2, 3, 6);     // Back 1
        // this.fBuffer.push(3, 6, 7);     // Back 2
        // this.fBuffer.push(0, 2, 4);     // Bottom 1
        // this.fBuffer.push(2, 4, 6);     // Bottom 2
        // this.fBuffer.push(1, 3, 5);     // Top 1
        // this.fBuffer.push(3, 5, 7);     // Top 2
        // this.numFaces = 12;
        this.fBuffer = [
          0,  1,  2,      0,  2,  3,    // front
          4,  5,  6,      4,  6,  7,    // back
          8,  9,  10,     8,  10, 11,   // top
          12, 13, 14,     12, 14, 15,   // bottom
          16, 17, 18,     16, 18, 19,   // right
          20, 21, 22,     20, 22, 23    // left
        ];
        this.numFaces = this.fBuffer.length/3;

        this.loadBuffers();
        console.log("SkyBox: Loaded buffers");

        this.cubeLoaded = true;
    }

    /**
     * Send the buffer objects to WebGL for rendering
     */
    loadBuffers() {
        this.VertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.vBuffer), gl.STATIC_DRAW);
        this.VertexPositionBuffer.itemSize = 3;
        this.VertexPositionBuffer.numItems = this.numVertices;

        this.IndexTriBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexTriBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(this.fBuffer), gl.STATIC_DRAW);
        this.IndexTriBuffer.itemSize = 1;
        this.IndexTriBuffer.numItems = this.fBuffer.length;
        console.log(this.IndexTriBuffer);
        console.log(this.fBuffer);
    }

    drawCube() {
        // Bind vertex buffer
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

        // Set the texture
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.cubeMap);
        gl.uniform1i(gl.getUniformLocation(shaderProgram, "uCubeSampler"), 0);

        // Draw
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexTriBuffer);
        gl.drawElements(gl.TRIANGLES, this.IndexTriBuffer.numItems, gl.UNSIGNED_INT, 0);
    }
}
